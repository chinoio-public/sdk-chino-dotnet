# [COMMUNITY] Chino .NET SDK <!-- omit in toc -->

C# SDK for Chino.io API on the .NET Framework

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

**Version**: `2.0.0`
 
**Useful links**
 - [Full SDK instructions](./INSTRUCTIONS.md)
 - [Chino.io API docs](http://console.test.chino.io/docs/v1)

For issues or questions, please contact [tech-support@chino.io](mailto:tech-support@chino.io).

-------------------------------------------------------------------------------
#### Table of content <!-- omit in toc -->
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage example](#usage-example)

-------------------------------------------------------------------------------

## Requirements
You will need the [NuGet](https://www.nuget.org/) package manager to install
this SDK.

## Installation
Install library Chino.dll:

    Install-Package Chino.dll

**NOTE:** The SDK can not be used in a `Universal Windows Application`.

## Usage example
Let's create a *Document* on Chino.io using the .NET SDK:

1. Create an API client with your Customer credentials for the 
   Chino.io platform:

    ```c#
    using Chino;

    ChinoAPI chino = new ChinoAPI("https://api.test.chino.io/v1", "your-Customer-ID", "your-Customer-Key");
    ```

2. Create a *Repository*:

    ```c#
    Repository repo = chino.repositories.create("Composers");
    ```

3. Create a *Schema* in the *Repository*:
   
    > Define the structure in a `class`:
    ```c#
    public class ClassicalComposers{
        public String name;
        public int century;
    }
    ```

    > create the *Schema* on Chino.io
    ```c#
    schema = chino.schemas.create(repo.repository_id, "Classical composers", typeof(ClassicalComposers));
    ```

4. Create your first *Document* in the *Schema*:
    ```c#
    Dictionary<String, Object> docContent = new Dictionary<string, object>();
    docContent.Add("name", "W. A. Mozart");
    docContent.Add("century", 1700);

    Document doc = chino.documents.create(docContent, schema.schema_id);
    
    Console.WriteLine("Created Document '" + doc.document_id + "' with content:");
    Console.WriteLine(doc.content);
    ```

*See more specific instructions about the SDK [here](./INSTRUCTIONS.md)*.