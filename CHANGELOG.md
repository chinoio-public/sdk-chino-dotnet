# Changelog

## [next release date] - next release



## [2020-02-01] - v 2.0.0
- Migrated code from GitHub

## [2019-04-17] - v 1.0.2
- Marked old Search API as `Obsolete` with a warning shown on compilation.
- Added new Search API

## [2019-03-11] - alpha.2
- Upgraded RestSharp dependency
- Added ChinoApiException and fixed project structure
- Added test resources for BLOBs test
- Fixed test suite

## [2019-02-28] - alpha - ***Pre-release.***
- Released main features of the SDK:
  - Chino API client
  - Applications
  - Auth
  - Blobs
  - Collections
  - Documents
  - Groups
  - Permissions
  - Repositories
  - Schemas
  - Search
  - UserSchemas
  - Users
- Created a suite of basic unit tests
