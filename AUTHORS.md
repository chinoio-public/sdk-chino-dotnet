# SDK Contributors

- Andrea Arighi (andrea@chino.io), Chino.io
- Paolo Prem (prempaolo@gmail.com), Chino.io
- Stefano Tranquillini (stefano.tranquillini@gmail.com), Chino.io

- - -

*Add your info on the bottom of the list. Suggested format:*

    - name, (email), company
