# [COMMUNITY] Chino .NET SDK <!-- omit in toc -->

C# SDK for Chino.io API on the .NET Framework

**Community SDK:** Most of the content and the code has been provided 
by third-parties.

*Read the [full API docs](https://console.test.chino.io/docs/v1)*

[*See changelog*](./CHANGELOG.md)

For issues or questions, please contact 
[tech-support@chino.io](mailto:tech-support@chino.io).

- - -
## Table of Contents <!-- omit in toc -->
- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)
  - [Usage as a Customer](#usage-as-a-customer)
  - [Usage as a User](#usage-as-a-user)
  - [API calls](#api-calls)
  - [Enhanced Search API](#enhanced-search-api)
- [Test the SDK](#test-the-sdk)

- - -

## Requirements
You will need the [NuGet](https://www.nuget.org/) package manager to install
this SDK.

## Installation
Install library Chino.dll:

    Install-Package Chino.dll

**NOTE:** The SDK can not be used in a `Universal Windows Application`.

## Usage
To perform API keys you need an instance of the API client. There are two
ways of creating one:

- [as a Customer](#usage-as-a-customer): 
  no access restrictions, only for admin purposes

- [as a User](#usage-as-a-user): 
  access to the resources is regulated by granular Permissions

### Usage as a Customer
Customers authenticate with their Customer credentials of the 
Chino.io platform:

```c#
using Chino;

baseUrl = "https://api.test.chino.io/v1"
customerId = "your-Customer-ID"
customerKey = "your-Customer-Key"

ChinoAPI chino = new ChinoAPI(baseUrl, customerId, customerKey);
```

**NOTE**: this authentication grants unrestricted access to any resource 
on your Chino.io account. **Never use it on client-side code**, e.g. a mobile 
or Desktop app.

`base_url` may be:
- `https://api.test.chino.io/v1` for the **Test API**
- `https://api.chino.io/v1` for the **Production API**

### Usage as a User
This is the most common scenario in Desktop / mobile applications.

To learn about the authentication process in detail, please refer to our 
[tutorial](https://console.test.chino.io/docs/tutorials/tutorial-auth) 
and to the 
[Chino.io API docs](https://console.test.chino.io/docs/v1#user-authentication)

Summarizing, the process is the following:

1. User register with `username` and `password`, which are saved in 
a Chino.io *User* object

2. Create an instance of the API client without credentials:

    ```c#
    using Chino;
    
    baseUrl = "https://api.test.chino.io/v1"
    
    ChinoAPI chino = new ChinoAPI(baseUrl);
    ```

    `base_url` may be:
    - `https://api.test.chino.io/v1` for the **Test API**
    - `https://api.chino.io/v1` for the **Production API**

3. Create an *Application* on Chino.io:

    ```c#
    Application app = chino.applications.create("my Application", "password", client_type: "public")
    ```

    when not running on a back-end, the *Application* **must** have 
    `client_type: "public"`

4. Users will login to your software with their `username`/`password`.

    You will need to send those credentials along with the `application_id`
    of the *Application*.
    You will receive a temporary **`access_token`** for the user:

    ```c#
    LoggedUser loggedUser = chino.auth.loginUserWithPassword("username", "password", app.app_id);
    token = logged_user.access_token;
    ```

5. Use the `access_token` to perform API calls on behalf of that user from 
    now on:

    ```c#
    chino.initClient(baseUrl, token);
    ```

### API calls
From the API client, you can manage all of your Chino.io resources:

```c#
offset = 0;
GetRepositoriesResponse response = chino.repositories.list(offset);
```

If the call is successful, the API call will return a JSON, as described
in the [full API docs](https://console.test.chino.io/docs/v1).
This will be converted by the SDK in the appropriate object, so you will be 
able to access it in an OOP-consistent fashion:

```c#
total_results = response.total_count
List<Repository> results = response.repositories
```

If the server returns an error, an instance of `ChinoApiException` will
be thrown by the SDK, with some information about the error.

### Enhanced Search API
The new *Search* API offer an easier interface to execute more complex 
Search queries. They will replace the old *Search* in a future release.

*Learn more in the [Chino.io API docs](https://console.test.chino.io/docs/v1#search-api).*

Here is how they work:

1. Create a *Search* query.

    This example will return all *Documents* in the specified *Schema*
    that have an `integer_field` value between **123** (not included) and 
    **200** (included):

    ```c#
    DocumentsSearch query = chino.search.documents("<schema_id>")
        // only return IDs of the Documents
        .setResultType(ResultTypeEnum.OnlyId)
        // sort in ascending order by the value of 'integer_field'
        .addSortRule("integer_field", OrderEnum.Asc)
        // specify the Search query: 
        // (integer_field > 123) AND (integer_field <= 200)
        .with("integer_field", FilterOperator.filter(FilterOperatorEnum.GreaterThan), 123)
        .and("integer_field", FilterOperator.filter(FilterOperatorEnum.LowerEqual), 200)
        // return the query instance
        .buildSearch()
    ```

    **NOTE**: once built, queries can be cached and executed multiple times.
    
2. Execute the query:
    ```c#
    GetDocumentsResponse results = query.execute();
    ```

3. Read results:
    ```c#
    Console.WriteLine($"Read { result.count } documents of { result.total_count } total results.");
    List<Documents> matchingDocuments = results.documents;
    ```

## Test the SDK
A simple test suite is provided, which can be found in `ChinoTest`.

The suite currently has two files:
- `UnitTest1.cs` with all the unit tests from older versions
- `SearchUnitTest.cs` with the unit tests for *Search*

***WARNING: running Unit Tests will delete everything on the 
Chino.io account!*** 

If you still want to run the test, add `automated_test=dotnet` 
to your environment variables.
You will also need to set the values of `customer_id` and `customer_key`.

You may optionally set the value of `host`, otherwise it will default
to `https://api.test.chino.io/v1`, which should work for most of the cases
(mind the **`/v1`** at the end).
